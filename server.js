var express     = require('express');
var fs          = require('fs');
var request     = require('request');
var app         = express();
var sqlite3     = require("sqlite3").verbose();
var file        = __dirname + '/data/hb_crawler.sqlite';
var db          = new sqlite3.Database(file);

app.get('/all', function (req, res) {
    db.all("SELECT * FROM db_crawlers where is_crawled=?",[], function(err, rows) {
        if (rows.length > 0) {
            res.json(rows);
        } else {
            res.json({'error_message':"rows not found.."});
        }
    });
});

app.get('/get/:id', function (req, res) {
    db.get('SELECT * FROM db_crawlers where id=?',[req.params.id], function(err, row) {
        if (typeof row  != 'undefined') {
            res.json(row);
        } else {
            res.json({'error_message':"row not found.."});
        }
    });
});

app.listen(3000, function () {
    console.log('api listening on port 3000!');
    console.log("get all rows endpoint :  /all");
    console.log("get (id) a row endpoint :  /get/:id");
});